package com.universidad.config.security.interceptors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import com.universidad.core.util.cryptografia.CryptoParameterAES;
@Component
public class CryptografyInterceptor implements Filter{
	@Value("${enable-security}")
	private boolean security;
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		if(this.security) {
			ParameterRequestWrapper wrapper = new ParameterRequestWrapper((HttpServletRequest)request);
			chain.doFilter(wrapper, response);
		}else {
			chain.doFilter(request, response);
		}
		

		//wrapper.addAllParameters(parametrosDesencripados);
		
		
	}



}
