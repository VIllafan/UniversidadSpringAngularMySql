package com.universidad.config.database;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
public class DataSourceConfig {
	private static final Logger LOG = LogManager.getLogger(DataSourceConfig.class);
	@Value("${enviroment}")
	private String enviroment;
	@Value("${enable-security}")
	private boolean security;

//declarar el datasource
	@Primary
	@Bean(name = "IxtapalucaDateSource")
	public DataSource ixtapalucaDataSource() {
		if(security) {
			LOG.info("@SGU==> Estamos en modo Seguro");
		}else {
			LOG.info("@SGU==> NO Estamos en modo Seguro");
		}
		DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
		LOG.info("@SGU====>Configurando la base de datos Ixtapaluca Configurarndo el ambiente : " + this.enviroment);
		switch (this.enviroment) {
		case "DES":
			driverManagerDataSource.setDriverClassName(EDataBaseConnectionConfig.PLANTEL_IXTAPALUCA_DES.getDriver());
			driverManagerDataSource.setUrl(EDataBaseConnectionConfig.PLANTEL_IXTAPALUCA_DES.getUrl());
			driverManagerDataSource.setUsername(EDataBaseConnectionConfig.PLANTEL_IXTAPALUCA_DES.getUsername());
			driverManagerDataSource.setPassword(EDataBaseConnectionConfig.PLANTEL_IXTAPALUCA_DES.getPassword());
			return driverManagerDataSource;
		case "TEST":
			driverManagerDataSource.setDriverClassName(EDataBaseConnectionConfig.PLANTEL_IXTAPALUCA_TEST.getDriver());
			driverManagerDataSource.setUrl(EDataBaseConnectionConfig.PLANTEL_IXTAPALUCA_TEST.getUrl());
			driverManagerDataSource.setUsername(EDataBaseConnectionConfig.PLANTEL_IXTAPALUCA_TEST.getUsername());
			driverManagerDataSource.setPassword(EDataBaseConnectionConfig.PLANTEL_IXTAPALUCA_TEST.getPassword());
			return driverManagerDataSource;
		case "PROD":
			driverManagerDataSource.setDriverClassName(EDataBaseConnectionConfig.PLANTEL_IXTAPALUCA_PROD.getDriver());
			driverManagerDataSource.setUrl(EDataBaseConnectionConfig.PLANTEL_IXTAPALUCA_PROD.getUrl());
			driverManagerDataSource.setUsername(EDataBaseConnectionConfig.PLANTEL_IXTAPALUCA_PROD.getUsername());
			driverManagerDataSource.setPassword(EDataBaseConnectionConfig.PLANTEL_IXTAPALUCA_PROD.getPassword());
			return driverManagerDataSource;
		}
		return driverManagerDataSource;
	}
	
	
	@Bean(name = "chalcoDataSource")
	public DataSource chalcoDataSource() {
		DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
		LOG.info("@SGU====>Configurando la base de datos Chalco Configurarndo el ambiente : " + this.enviroment);
		switch (this.enviroment) {
		case "DES":
			driverManagerDataSource.setDriverClassName(EDataBaseConnectionConfig.PLANTEL_CHALCO_DES.getDriver());
			driverManagerDataSource.setUrl(EDataBaseConnectionConfig.PLANTEL_CHALCO_DES.getUrl());
			driverManagerDataSource.setUsername(EDataBaseConnectionConfig.PLANTEL_CHALCO_DES.getUsername());
			driverManagerDataSource.setPassword(EDataBaseConnectionConfig.PLANTEL_CHALCO_DES.getPassword());
			return driverManagerDataSource;
		case "TEST":
			driverManagerDataSource.setDriverClassName(EDataBaseConnectionConfig.PLANTEL_CHALCO_TEST.getDriver());
			driverManagerDataSource.setUrl(EDataBaseConnectionConfig.PLANTEL_CHALCO_TEST.getUrl());
			driverManagerDataSource.setUsername(EDataBaseConnectionConfig.PLANTEL_CHALCO_TEST.getUsername());
			driverManagerDataSource.setPassword(EDataBaseConnectionConfig.PLANTEL_CHALCO_TEST.getPassword());
			return driverManagerDataSource;
		case "PROD":
			driverManagerDataSource.setDriverClassName(EDataBaseConnectionConfig.PLANTEL_CHALCO_PROD.getDriver());
			driverManagerDataSource.setUrl(EDataBaseConnectionConfig.PLANTEL_CHALCO_PROD.getUrl());
			driverManagerDataSource.setUsername(EDataBaseConnectionConfig.PLANTEL_CHALCO_PROD.getUsername());
			driverManagerDataSource.setPassword(EDataBaseConnectionConfig.PLANTEL_CHALCO_PROD.getPassword());
			return driverManagerDataSource;
		}
		return driverManagerDataSource;
	}
	@Bean(name="dataSource")
	public DataSource dataSource() {
		Map<Object, Object> dataSourcceMap = new HashMap<>();
		dataSourcceMap.put(DataBaseContext.IXTAPALUCA, ixtapalucaDataSource());
		dataSourcceMap.put(DataBaseContext.CHALCO, chalcoDataSource());
		MultiRoutingDataSource multiRoutingDataSource = new MultiRoutingDataSource();
		multiRoutingDataSource.setDefaultTargetDataSource(ixtapalucaDataSource());
		multiRoutingDataSource.setTargetDataSources(dataSourcceMap);
		return multiRoutingDataSource;
	}
	@Bean("entityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entitymanager() {
		LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
		entityManagerFactoryBean.setDataSource(dataSource());
		entityManagerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter());
		entityManagerFactoryBean.setPackagesToScan("com.universidad.core.domain.entity.*");
		entityManagerFactoryBean.setJpaProperties(hibernatePropierties());
		return entityManagerFactoryBean;
	}
	
	
	
	
	@Bean(name="transactionManager")
	public PlatformTransactionManager multiTransactionManager() {
		JpaTransactionManager manager = new JpaTransactionManager();
		manager.setEntityManagerFactory(entitymanager().getObject());
		return manager;
	}

	private Properties hibernatePropierties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect", "org.hibernate.dialect.MySQL5InnoDBDialect");
		properties.put("hibernate.ddl-auto", "update");
		return properties;
	}
	private HibernateJpaVendorAdapter jpaVendorAdapter() {
		HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
		adapter.setGenerateDdl(true);
		adapter.setShowSql(true);
		return adapter;
	}

}
