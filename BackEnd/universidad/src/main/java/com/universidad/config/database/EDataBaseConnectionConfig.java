package com.universidad.config.database;

public enum EDataBaseConnectionConfig {
	PLANTEL_IXTAPALUCA_DES("jdbc:mysql://localhost:3306/universidad_plantel_ixtapaluca", "root", "admin", "com.mysql.cj.jdbc.Driver"),
	PLANTEL_IXTAPALUCA_TEST("jdbc:mysql://localhost:3306/universidad_plantel_ixtapaluca", "root", "admin", "com.mysql.cj.jdbc.Driver"),
	PLANTEL_IXTAPALUCA_PROD("jdbc:mysql://localhost:3306/universidad_plantel_ixtapaluca", "root", "admin", "com.mysql.cj.jdbc.Driver"),
	PLANTEL_CHALCO_DES("jdbc:mysql://localhost:3306/universidad_plantel_chalco", "root", "admin", "com.mysql.cj.jdbc.Driver"),
	PLANTEL_CHALCO_TEST("jdbc:mysql://localhost:3306/universidad_plantel_chalco", "root", "admin", "com.mysql.cj.jdbc.Driver"),
	PLANTEL_CHALCO_PROD("jdbc:mysql://localhost:3306/universidad_plantel_chalco", "root", "admin", "com.mysql.cj.jdbc.Driver");

	private String url;
	private String password;
	private String username;
	private String driver;

	private EDataBaseConnectionConfig(String url, String username, String password, String driver) {
		this.url = url;
		this.password = password;
		this.username = username;
		this.driver = driver;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

}
