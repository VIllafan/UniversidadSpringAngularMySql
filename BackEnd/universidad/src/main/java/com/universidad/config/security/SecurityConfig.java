package com.universidad.config.security;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.universidad.core.services.sercurity.accesCredentialService.AccessCredentialService;
import com.universidad.core.util.cryptografia.CryptoPasswordAES;
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	@Autowired
	AccessCredentialService accessCrendetialService;
	protected String[] urlsAutorizadas =
		{
				"/sgu/C01Usuario/registerNewUserAsAlumno"};
	@Override
	protected void configure(AuthenticationManagerBuilder builder) throws Exception{
		builder.userDetailsService(accessCrendetialService);
	}
	
	protected void configure(HttpSecurity http)throws Exception{
		http.csrf().disable();
		http.authorizeRequests().antMatchers(urlsAutorizadas).permitAll().anyRequest().authenticated()
		.and().cors().and().addFilterBefore(
				new LoginFilter("/sgu/security/authentication", authenticationManager()),
				UsernamePasswordAuthenticationFilter.class).
		addFilterBefore(new JwtFilter(), UsernamePasswordAuthenticationFilter.class);
		http.cors().configurationSource(corsConfigurationSource());
	}
	
	
    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.addAllowedHeader("*");
        configuration.addAllowedMethod("*");
        configuration.addAllowedOrigin("*");
        configuration.setAllowedOrigins(Arrays.asList("*"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return  source;
    }
	
	
	
	@Bean public BCryptPasswordEncoder bCryptPasswordEncoder() {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(31);
		String test = "Hola123$";
		System.out.println(test);
		test = CryptoPasswordAES.aesEncrypt(test);
		System.out.println(test);
		test = encoder.encode(test);
		System.out.println(test);
		return encoder;
	}

}
