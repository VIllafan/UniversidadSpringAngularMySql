package com.universidad.config.security;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.universidad.core.domain.dto.LoginDTO;
import com.universidad.core.util.cryptografia.CryptoParameterAES;
import com.universidad.core.util.cryptografia.CryptoPasswordAES;

public class LoginFilter extends AbstractAuthenticationProcessingFilter {
	@Value("${enable-security:true}")
	private boolean security;
	
	protected LoginFilter(String defaultFilterProcessesUrl, AuthenticationManager authenticationManager) {
		super(new AntPathRequestMatcher(defaultFilterProcessesUrl));
		setAuthenticationManager(authenticationManager);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		InputStream inputStream = request.getInputStream();
		LoginDTO loginDTO = new ObjectMapper().readValue(inputStream, LoginDTO.class);
		loginDTO = this.desencriptarLogin(loginDTO);
		Authentication authentication = getAuthenticationManager().authenticate(new UsernamePasswordAuthenticationToken(
				loginDTO.getUsername() , loginDTO.getPassword()
				));
		return authentication;
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) {
		List<GrantedAuthority> authorities = new ArrayList<>(authResult.getAuthorities());
		JwtUtil.addAuthentication(response, authResult.getName(), authorities);
	}

	private LoginDTO desencriptarLogin(LoginDTO dto) {
		if(true) {
			LoginDTO loginDTO = new LoginDTO();
			String desecryptedUsername;
			String desecryptedPassword;
			String encryptedPassword;
			String passwordPlain;
			try {
				desecryptedPassword = new String(Base64.getDecoder().decode(dto.getPassword()));
				desecryptedUsername = new String(Base64.getDecoder().decode(dto.getUsername()));
				passwordPlain =CryptoParameterAES.aesDecrypt(desecryptedPassword);
				encryptedPassword = CryptoPasswordAES.aesEncrypt(passwordPlain);
				loginDTO.setPassword(encryptedPassword);
				loginDTO.setUsername(CryptoParameterAES.aesDecrypt(desecryptedUsername));
				return loginDTO;

			} catch (Exception e) {
				throw e;
			}	
		}else {
			return dto;
		}
		
	}

}
