package com.universidad.config.security;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import com.google.gson.Gson;
import com.universidad.core.util.cryptografia.CryptoParameterAES;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JwtUtil {
	private static final Logger LOG = LogManager.getFormatterLogger(JwtFilter.class);
	private static final String SECRET_CODE = "AykbeZQOnBEpLH";

	static void addAuthentication(HttpServletResponse response, String username, List<GrantedAuthority> authorities) {
		JwtBuilder builder = Jwts.builder().setSubject(CryptoParameterAES.aesEncrypt(username))
				.signWith(SignatureAlgorithm.HS512, SECRET_CODE);
		builder.claim("Authority", CryptoParameterAES.aesEncrypt(authorities.get(0).getAuthority()));
		builder.claim("Rol", CryptoParameterAES.aesEncrypt(authorities.get(1).getAuthority()));
		builder.claim("DataBaseContext", CryptoParameterAES.aesEncrypt(authorities.get(2).getAuthority()));
		builder.setExpiration(getExpiration());
		String strToken = builder.compact();
		response.addHeader("Authorization", strToken);
		try {
			String responseJson = new Gson().toJson(strToken);
			response.getWriter().write(responseJson);
		} catch (Exception e) {
			LOG.info("@SGU====>Ocurrio un error durante la generacion del TOKEN: " + e.getMessage());
		}
	}

	public static Authentication getAuthentication(HttpServletRequest request, HttpServletResponse response) {
		try {
			String token = request.getHeader("Authorization");
			if (token != null) {
				String user = Jwts.parser().setSigningKey(SECRET_CODE).parseClaimsJws(token.replace("Bearer", ""))
						.getBody().getSubject();
				user = CryptoParameterAES.aesDecrypt(user);
				return user != null ? new UsernamePasswordAuthenticationToken(user, null, Collections.emptyList())
						: null;
			}
		} catch (Exception e) {
			LOG.info("@SGU--->Token expired :" + e.getCause());
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
		}
		return null;
	}

	private static Date getExpiration() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, 30);
		return calendar.getTime();
	}
}
