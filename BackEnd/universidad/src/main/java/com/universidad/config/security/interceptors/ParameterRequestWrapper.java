package com.universidad.config.security.interceptors;

import java.util.Base64;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.universidad.core.util.cryptografia.CryptoParameterAES;



public class ParameterRequestWrapper extends HttpServletRequestWrapper{
	 private static final Logger LOG = LogManager.getLogger();

	private Map<String, String[]>parameterMap;
	 
		public ParameterRequestWrapper(HttpServletRequest request) {
			super(request);
		
				this.parameterMap = desencriptarParametros(request);
			
			 
		}
	 private Map<String, String[]> desencriptarParametros(HttpServletRequest request) {
			
		 List<String> parametros = null;
		 parametros = Collections.list(request.getParameterNames());
		 Map<String , String[]> parametrosDesencripados = new  HashMap<String, String[]>();
			for(String param : parametros) {
				if(!param.equals("file")) {
					String encryptedParam;
					String decryptedParam;
					encryptedParam = new String(Base64.getDecoder().decode(request.getParameter(param)));
					decryptedParam = CryptoParameterAES.aesDecrypt(encryptedParam);
					LOG.info("Se Desencripto el paramentro "+param +" : "+request.getParameter(param) + " a : "+decryptedParam);
					parametrosDesencripados.put(param, new String[] {decryptedParam});
				}else {
					parametrosDesencripados.put(param,new String[] {request.getParameter(param)});
				}
				
			}
			return parametrosDesencripados;
		}
	
	
		 @Override
		public Enumeration<String> getParameterNames() {
			Vector<String> l = new Vector<String>(parameterMap.keySet());
			return l.elements();
		}
	 @Override
		public String[] getParameterValues(String name) {
			Object v = parameterMap.get(name);
			if (v == null) {
				return null;
			} else if (v instanceof String[]) {
				return (String[]) v;
			} else if (v instanceof String) {
				return new String[] { (String) v };
			} else {
				return new String[] { v.toString() };
			}
		}
	 @Override
		public String getParameter(String name) {
			Object v = parameterMap.get(name);
			if (v == null) {
				return null;
			} else if (v instanceof String[]) {
				String[] strArr = (String[]) v;
				if (strArr.length > 0) {
					return strArr[0];
				} else {
					return null;
				}
			} else if (v instanceof String) {
				return (String) v;
			} else {
				return v.toString();
			}
		}
	 
	 
}
