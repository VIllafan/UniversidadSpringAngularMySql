package com.universidad.config.database;

public class DataSourceContextHolder {
	private static final ThreadLocal<DataBaseContext> dbContextHolder = new ThreadLocal<>();

	public static void setCurrentDB(DataBaseContext dbType) {
		dbContextHolder.set(dbType);
	}

	public static DataBaseContext getCurrentyDataBase() {
		return dbContextHolder.get();
	}

	public void clear() {
		dbContextHolder.remove();
	}
}
