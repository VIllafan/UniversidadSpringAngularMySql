package com.universidad.core.util.Enum;

public enum EClavesT03Estatus {
ACTIVO(1),
INACTIVO(2),
BLOQUEADO(3),
APROVADO(4);
	
	
	private Integer valorInt;
 
	private EClavesT03Estatus(Integer valorInt) {
		this.valorInt = valorInt;
	}

	public Integer getValorInt() {
		return valorInt;
	}

	public void setValorInt(Integer valorInt) {
		this.valorInt = valorInt;
	}
	
	
}
