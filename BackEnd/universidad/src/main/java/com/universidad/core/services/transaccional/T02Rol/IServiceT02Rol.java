package com.universidad.core.services.transaccional.T02Rol;

import com.universidad.core.domain.entity.transaccional.T01Perfil;
import com.universidad.core.domain.entity.transaccional.T02Rol;

public interface IServiceT02Rol {
	T02Rol findById(Integer id);

	T02Rol findAllByPerfil(T01Perfil perfil);
}
