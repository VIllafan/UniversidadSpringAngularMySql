package com.universidad.core.domain.entity.transaccional;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.universidad.core.domain.entity.catalogo.C02Archivo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "T04TipoArchivo")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class T04TipoArchivo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "idT04TipoArchivo")
	private Integer idT04TipoArchivo;
	@Column(name = "nombre")
	private String nombre;
	@OneToMany(mappedBy = "tipoArchivoId" , fetch = FetchType.LAZY)
	private List<C02Archivo> archivos;
}
