package com.universidad.core.services.transaccional.T04TipoArchivo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.universidad.core.domain.entity.transaccional.T04TipoArchivo;
import com.universidad.core.repository.transaccional.IT04TipoArchivoRespository;

@Service
public class ServiceT04TipoArchivoImpl implements IServiceT04TipoArchivo{
@Autowired
private IT04TipoArchivoRespository iT04TipoArchivoRespository;
	@Override
	public T04TipoArchivo findById(Integer id) {
		// TODO Auto-generated method stub
		return this.iT04TipoArchivoRespository.findById(id).get();
	}

}
