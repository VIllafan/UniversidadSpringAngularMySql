package com.universidad.core.controller.catalogo.C01Usuario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.universidad.core.domain.entity.catalogo.C01Usuario;
import com.universidad.core.domain.entity.transaccional.T01Perfil;
import com.universidad.core.domain.entity.transaccional.T02Rol;
import com.universidad.core.domain.entity.transaccional.T03Estatus;
import com.universidad.core.services.catalogo.C01Usuario.IServiceC01Usuario;
import com.universidad.core.services.transaccional.T01Perfil.IServiceT01Perfil;
import com.universidad.core.services.transaccional.T02Rol.IServiceT02Rol;
import com.universidad.core.services.transaccional.T03Estatus.IServiceT03Estatus;
import com.universidad.core.util.Enum.EClavesT01Perfil;
import com.universidad.core.util.Enum.EClavesT02Rol;
import com.universidad.core.util.Enum.EClavesT03Estatus;
import com.universidad.core.util.claves.GeneradorDeClaves;
import com.universidad.core.util.cryptografia.CryptoPasswordAES;

@RestController
@CrossOrigin("*")
@RequestMapping("/sgu/C01Usuario")
public class C01UsusarioController {
@Autowired
private IServiceC01Usuario iServiceC01Usuario;
@Autowired
private IServiceT02Rol iServiceT02Rol;
@Autowired
private IServiceT01Perfil iServiceT01Perfil;
@Autowired
private IServiceT03Estatus iServiceT03Estatus;
@Autowired
private BCryptPasswordEncoder passwordEncoder;

	
	

@PostMapping("/registerNewUserAsAlumno")
ResponseEntity<?> registerNewUserAlumno(
		@RequestParam(name="correo" , required = false)String correo,
		@RequestParam(name = "nombre", required = false) String nombre,
		@RequestParam(name ="aPaterno", required = false)String aPaterno,
		@RequestParam(name ="aMaterno", required = false)String aMaterno
		){
	
	C01Usuario usuario;
	T01Perfil perfil;
	T02Rol rol;
	T03Estatus estatus;
	String passworddefault;
	String encryptedPassword;
	String matriculaTemp;
	try {
		perfil = this.iServiceT01Perfil.findById(EClavesT01Perfil.ALUMNO.getValorId());
		rol = this.iServiceT02Rol.findById(EClavesT02Rol.ASPIRANTE.getValorId());
		estatus = this.iServiceT03Estatus.findById(EClavesT03Estatus.ACTIVO.getValorInt());
		passworddefault = GeneradorDeClaves.generar(8, 40, 30, 30);
		matriculaTemp = GeneradorDeClaves.generar(12, 0, 0, 80);
		encryptedPassword = CryptoPasswordAES.aesEncrypt(passworddefault);
		usuario = C01Usuario.builder()
				.nombre(nombre)
				.aPaterno(aPaterno)
				.aMaterno(aMaterno)
				.correo(correo)
				.matricula(matriculaTemp)
				.password(this.passwordEncoder.encode(encryptedPassword))
				.perfilId(perfil)
				.rolId(rol)
				.estatusId(estatus)
				.build();
	usuario = this.iServiceC01Usuario.save(usuario);
	return new ResponseEntity<C01Usuario>(usuario , HttpStatus.OK);
		
	} catch (Exception e) {
		return new ResponseEntity<Object>(e.getCause() , HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	
}
@GetMapping("/findByCorreo")
public ResponseEntity<?> findByCorreo(@RequestParam("correo") String correo){
	C01Usuario c01Usuario;
	
	try {
	c01Usuario = this.iServiceC01Usuario.findByCorreo(correo);
	return new ResponseEntity<C01Usuario>(c01Usuario , HttpStatus.OK);
	} catch (Exception e) {
		return new ResponseEntity<Object>(e.getCause() , HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
	
}
