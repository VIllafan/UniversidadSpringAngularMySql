package com.universidad.core.services.catalogo.C01Usuario;

import com.universidad.core.domain.entity.catalogo.C01Usuario;

public interface IServiceC01Usuario {
C01Usuario findById(Integer id) throws Exception;
C01Usuario findByCorreo(String correo);
C01Usuario save(C01Usuario c01Usuario);


}
