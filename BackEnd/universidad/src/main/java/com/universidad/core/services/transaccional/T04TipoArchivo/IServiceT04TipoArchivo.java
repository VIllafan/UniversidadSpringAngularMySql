package com.universidad.core.services.transaccional.T04TipoArchivo;

import com.universidad.core.domain.entity.transaccional.T04TipoArchivo;

public interface IServiceT04TipoArchivo {
	T04TipoArchivo findById(Integer id);
}
