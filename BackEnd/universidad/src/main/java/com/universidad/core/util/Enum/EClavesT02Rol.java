package com.universidad.core.util.Enum;

public enum EClavesT02Rol {
ASPIRANTE(1),
SYSADMIN(2),
PROFESOR_TIEMPO_COMPLETO(3);
	
	
	private Integer valorId;

	public Integer getValorId() {
		return valorId;
	}

	public void setValorId(Integer valorId) {
		this.valorId = valorId;
	}

	private EClavesT02Rol(Integer valorId) {
		this.valorId = valorId;
	}
	
}
