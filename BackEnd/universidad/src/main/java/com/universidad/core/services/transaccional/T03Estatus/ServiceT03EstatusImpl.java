package com.universidad.core.services.transaccional.T03Estatus;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.universidad.core.domain.entity.transaccional.T03Estatus;
import com.universidad.core.repository.transaccional.IT03EstatusRepository;

@Service
public class ServiceT03EstatusImpl implements IServiceT03Estatus {
@Autowired
private IT03EstatusRepository iT03EstatusRepository;
	@Override
	public T03Estatus findById(Integer id) {
		// TODO Auto-generated method stub
		return this.iT03EstatusRepository.findById(id).get();
	}

}
