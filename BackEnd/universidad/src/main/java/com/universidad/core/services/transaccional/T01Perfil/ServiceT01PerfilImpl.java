package com.universidad.core.services.transaccional.T01Perfil;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.universidad.core.domain.entity.transaccional.T01Perfil;
import com.universidad.core.repository.transaccional.IT01PerfilRepository;

@Service
public class ServiceT01PerfilImpl implements IServiceT01Perfil {
	@Autowired
	private IT01PerfilRepository it01PerfilRepository;

	@Override
	public T01Perfil findById(Integer id) {
		return this.it01PerfilRepository.findById(id).get();
	}

	@Override
	public List<T01Perfil> findAll() {

		return null;
	}

}
