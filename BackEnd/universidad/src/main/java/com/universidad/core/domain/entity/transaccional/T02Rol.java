package com.universidad.core.domain.entity.transaccional;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.universidad.core.domain.entity.catalogo.C01Usuario;


@Entity
@Table(name = "T02Rol")
public class T02Rol implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "idT02Rol")
	private Integer idT02Rol;
	@Basic(optional = false)
	@Column(name = "nombre")
	private String nombre;
	@Basic(optional = false)
	@Column(name = "descripcion")
	private String descripcion;
	@OneToMany(mappedBy = "rolId" , fetch = FetchType.LAZY)
	private List<C01Usuario> usuarios; 
	
	@JoinColumn(name="perfilId")
	@ManyToOne(optional = false ,cascade = CascadeType.ALL , fetch = FetchType.LAZY)
	private T01Perfil perfilId;
	
	
	
	
	public T02Rol() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}

	public Integer getIdT02Rol() {
		return idT02Rol;
	}

	public void setIdT02Rol(Integer idT02Rol) {
		this.idT02Rol = idT02Rol;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<C01Usuario> getUsusarios() {
		return usuarios;
	}

	public void setUsusarios(List<C01Usuario> ususarios) {
		this.usuarios = ususarios;
	}

	public T01Perfil getPerfilId() {
		return perfilId;
	}

	public void setPerfilId(T01Perfil perfilId) {
		this.perfilId = perfilId;
	}
	
}
