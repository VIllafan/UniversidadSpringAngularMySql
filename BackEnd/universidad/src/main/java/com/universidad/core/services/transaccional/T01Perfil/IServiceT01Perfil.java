package com.universidad.core.services.transaccional.T01Perfil;

import java.util.List;

import com.universidad.core.domain.entity.transaccional.T01Perfil;

public interface IServiceT01Perfil {
	T01Perfil findById(Integer id);

	List<T01Perfil> findAll();
}
