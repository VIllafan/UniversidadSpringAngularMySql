package com.universidad.core.repository.transaccional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.universidad.core.domain.entity.catalogo.C01Usuario;
import com.universidad.core.domain.entity.transaccional.T01Perfil;

@Repository
public interface IT01PerfilRepository extends JpaRepository<T01Perfil, Integer>{
	@Query("select c from T01Perfil c join fetch  c.usuarios u where u=:c01usuario")
	public T01Perfil findByC01Usuario(@Param("c01usuario") C01Usuario ususario);

}
