package com.universidad.core.repository.catalogo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.universidad.core.domain.entity.catalogo.C01Usuario;

@Repository
public interface IC01UsuarioRepository extends JpaRepository<C01Usuario, Integer>{

	public C01Usuario findByCorreo(String correo);
}
