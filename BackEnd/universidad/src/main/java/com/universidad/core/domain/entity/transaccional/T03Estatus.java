package com.universidad.core.domain.entity.transaccional;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.universidad.core.domain.entity.catalogo.C01Usuario;
import com.universidad.core.domain.entity.catalogo.C02Archivo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Entity
@Table(name ="T03Estatus")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class T03Estatus  implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name="idT03Estatus")
	private Integer idT03Estatus;
	@Basic(optional = false)
	@Column(name="nombre")
	private String nombre;
	@JsonIgnore
	@OneToMany(mappedBy = "estatusId" , fetch = FetchType.LAZY)
	private List<C01Usuario> ususarios;
	@JsonIgnore
	@OneToMany(mappedBy = "estatusId" , fetch = FetchType.LAZY)
	private List<C02Archivo> archivos;
	
	





	

}
