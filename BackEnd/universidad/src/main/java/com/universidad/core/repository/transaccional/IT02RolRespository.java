package com.universidad.core.repository.transaccional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.universidad.core.domain.entity.catalogo.C01Usuario;
import com.universidad.core.domain.entity.transaccional.T02Rol;

@Repository
public interface IT02RolRespository extends JpaRepository<T02Rol, Integer>{

	@Query("select t from T02Rol t join fetch t.usuarios u where u =:usuario")
	public T02Rol findFromC01Usuario (@Param("usuario") C01Usuario usuario);
}
