package com.universidad.core.services.sercurity.accesCredentialService;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.universidad.config.database.DataBaseContext;
import com.universidad.config.database.DataSourceContextHolder;
import com.universidad.core.domain.entity.catalogo.C01Usuario;
import com.universidad.core.domain.entity.transaccional.T01Perfil;
import com.universidad.core.domain.entity.transaccional.T02Rol;
import com.universidad.core.repository.catalogo.IC01UsuarioRepository;
import com.universidad.core.repository.transaccional.IT01PerfilRepository;
import com.universidad.core.repository.transaccional.IT02RolRespository;

@Service
public class AccessCredentialService implements UserDetailsService {
	@Autowired
	private IC01UsuarioRepository iC01UsuarioRepository;
	@Autowired
	private IT01PerfilRepository iT01PerfilRepository;
	@Autowired
	private IT02RolRespository iT02RolRepository;
	

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		C01Usuario usuario;
		
		try {
			usuario = this.findUsuario(username);
			if(usuario != null) {
				List<GrantedAuthority> authorities = this.buildAuthorities(usuario);
				if(authorities != null  && !authorities.isEmpty()) {
					return new User(usuario.getCorreo(),usuario.getPassword(),true,true,true,true,authorities);
				}else {
					throw new Exception("Error al crear autorities ");
				}
				
			}else {
				throw new UsernameNotFoundException("No se encrontro el usuario con el correo : "+username);
			} 
		} catch (Exception e) {
			
		}
		return null;
	}
	
	private C01Usuario findUsuario(String correo) {
		C01Usuario usuario = null;
		for(DataBaseContext context : DataBaseContext.values()) {
			DataSourceContextHolder.setCurrentDB(context);
			usuario = this.iC01UsuarioRepository.findByCorreo(correo);
			if(usuario!=null) {
				return usuario;
			}
		}
		return usuario;
	}
	
	private List<GrantedAuthority> buildAuthorities(C01Usuario c01Ususario){
		List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
		T01Perfil perfil = this.iT01PerfilRepository.findByC01Usuario(c01Ususario);
		T02Rol rol =this.iT02RolRepository.findFromC01Usuario(c01Ususario);
		if(perfil!=null) {
			grantedAuthorities.add(new SimpleGrantedAuthority(perfil.getNombre()));
			grantedAuthorities.add(new SimpleGrantedAuthority(rol.getNombre()));
			if(DataSourceContextHolder.getCurrentyDataBase()==DataBaseContext.IXTAPALUCA) {
				grantedAuthorities.add(new SimpleGrantedAuthority("IX"));
			}else {
				grantedAuthorities.add(new SimpleGrantedAuthority("CH"));
			}
		}
		return grantedAuthorities;
	}

}
