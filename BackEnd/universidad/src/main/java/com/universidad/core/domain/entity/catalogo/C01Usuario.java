package com.universidad.core.domain.entity.catalogo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.universidad.core.domain.entity.transaccional.T01Perfil;
import com.universidad.core.domain.entity.transaccional.T02Rol;
import com.universidad.core.domain.entity.transaccional.T03Estatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "C01Usuario")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class C01Usuario implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name="idC01Usuario")
	private Integer idUsuario;
	@Column(name="nombre")
	@Basic(optional = false)
	private String nombre;
	@Column(name="aPaterno")
	@Basic(optional = false)
	private String aPaterno;
	@Column(name="aMaterno")
	@Basic(optional = false)
	private String aMaterno;
	@JsonIgnore
	@Column(name="password")
	@Basic(optional = false)
	private String password;
	@Column(name = "correo" ,unique = true)
	@Basic(optional = false)
	private String correo;
	@Column(name="matricula")
	@Basic(optional = false)
	private String matricula;
	@JsonIgnore
	@JoinColumn(name = "perfilId" )
	@ManyToOne(optional = false , cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	private T01Perfil perfilId;
	@JsonIgnore
	@JoinColumn(name="rolId")
	@ManyToOne(optional = false, cascade = CascadeType.ALL , fetch = FetchType.LAZY)
	private T02Rol rolId;
	@JsonIgnore
	@JoinColumn(name="estatusId")
	@ManyToOne(optional = true , cascade = CascadeType.ALL , fetch = FetchType.LAZY)
	private T03Estatus estatusId;
	@JsonIgnore
	@OneToMany(mappedBy = "usuarioId" ,fetch = FetchType.LAZY)
	private List<C02Archivo> archivos;
	
	
	
	public C01Usuario(Integer idUsuario, String nombre, String aPaterno, String aMaterno, String password) {
		super();
		this.idUsuario = idUsuario;
		this.nombre = nombre;
		this.aPaterno = aPaterno;
		this.aMaterno = aMaterno;
		this.password = password;
	}
	public Integer getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getaPaterno() {
		return aPaterno;
	}
	public void setaPaterno(String aPaterno) {
		this.aPaterno = aPaterno;
	}
	public String getaMaterno() {
		return aMaterno;
	}
	public void setaMaterno(String aMaterno) {
		this.aMaterno = aMaterno;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	public T01Perfil getPerfilId() {
		return perfilId;
	}
	public void setPerfilId(T01Perfil perfilId) {
		this.perfilId = perfilId;
	}
	public T02Rol getRolId() {
		return rolId;
	}
	public void setRolId(T02Rol rolId) {
		this.rolId = rolId;
	}
	
	
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public T03Estatus getEstatusId() {
		return estatusId;
	}
	public void setEstatusId(T03Estatus estatusId) {
		this.estatusId = estatusId;
	}
	@Override
	public String toString() {
		return "C01Ususario [idUsuario=" + idUsuario + ", nombre=" + nombre + ", aPaterno=" + aPaterno + ", aMaterno="
				+ aMaterno + ", password=" + password + "]";
	}

	
	
	
}
