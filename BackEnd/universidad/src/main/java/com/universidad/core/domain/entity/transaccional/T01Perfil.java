package com.universidad.core.domain.entity.transaccional;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.universidad.core.domain.entity.catalogo.C01Usuario;

@Entity
@Table(name ="T01Perfil")
public class T01Perfil implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name="idT01Perfil")
	private Integer idT01Perfil;
	@Basic(optional = false)
	@Column(name="nombre")
	private String nombre;
	
	@OneToMany(mappedBy = "perfilId" , fetch = FetchType.LAZY)
	private List<C01Usuario> usuarios;
	
	@OneToMany(mappedBy = "perfilId", fetch = FetchType.LAZY)
	private List<T02Rol> rols;
	
	public T01Perfil() {
		// TODO Auto-generated constructor stub
	}


	public T01Perfil(Integer idT01Perfil, String nombre) {
		super();
		this.idT01Perfil = idT01Perfil;
		this.nombre = nombre;
	}


	public Integer getIdT01Perfil() {
		return idT01Perfil;
	}


	public void setIdT01Perfil(Integer idT01Perfil) {
		this.idT01Perfil = idT01Perfil;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public List<C01Usuario> getUsuarios() {
		return usuarios;
	}


	public void setUsuarios(List<C01Usuario> usuarios) {
		this.usuarios = usuarios;
	}


	public List<T02Rol> getRols() {
		return rols;
	}


	public void setRols(List<T02Rol> rols) {
		this.rols = rols;
	}
	
	
}
