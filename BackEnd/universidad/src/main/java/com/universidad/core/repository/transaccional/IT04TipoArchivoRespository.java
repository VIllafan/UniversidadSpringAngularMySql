package com.universidad.core.repository.transaccional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.universidad.core.domain.entity.transaccional.T04TipoArchivo;

@Repository
public interface IT04TipoArchivoRespository extends JpaRepository<T04TipoArchivo, Integer>{

}
