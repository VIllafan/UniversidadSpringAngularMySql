package com.universidad.core.repository.catalogo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.universidad.core.domain.entity.catalogo.C02Archivo;

@Repository
public interface IC02ArchivoRepository extends JpaRepository<C02Archivo, Integer>{

}
