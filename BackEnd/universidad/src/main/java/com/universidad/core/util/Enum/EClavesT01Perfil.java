package com.universidad.core.util.Enum;

public enum EClavesT01Perfil {
ALUMNO(1),
PROFESOR(2),
ADMINISTRADOR(3);


private Integer valorId;
	private EClavesT01Perfil(Integer id) {
		this.valorId = id;
	}
	public Integer getValorId() {
		return valorId;
	}
	public void setValorId(Integer valorId) {
		this.valorId = valorId;
	}
	
}
