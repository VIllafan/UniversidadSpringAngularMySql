package com.universidad.core.services.catalogo.C01Usuario;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.universidad.core.domain.entity.catalogo.C01Usuario;
import com.universidad.core.repository.catalogo.IC01UsuarioRepository;

@Service
public class ServiceC01UsuarioImpl implements IServiceC01Usuario{
@Autowired
private IC01UsuarioRepository iC01UsusarioRepository;
	
	@Override
	public C01Usuario findById(Integer id) throws Exception {
		// TODO Auto-generated method stub
		C01Usuario c01Usuario = null;
		try {
			c01Usuario = this.iC01UsusarioRepository.findById(id).get();
		} catch (NoSuchElementException e) {
			throw new Exception("No se encontro ningun usuario con el Id : "+id);
		}
		return c01Usuario;
	}

	@Override
	public C01Usuario findByCorreo(String correo) {
		
		return this.iC01UsusarioRepository.findByCorreo(correo);
	}

	@Override
	public C01Usuario save(C01Usuario c01Usuario) {
	
		return this.iC01UsusarioRepository.save(c01Usuario);
	}

}
