package com.universidad.core.services.catalogo.C02Archvo;

import com.universidad.core.domain.entity.catalogo.C02Archivo;

public interface IServiceC02Archivo {
public C02Archivo findById(Integer id);
public C02Archivo save(C02Archivo archivo);
public String getMineType(String fileName);
}
