package com.universidad.core.repository.transaccional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.universidad.core.domain.entity.transaccional.T03Estatus;

@Repository
public interface IT03EstatusRepository extends JpaRepository<T03Estatus, Integer> {

}
