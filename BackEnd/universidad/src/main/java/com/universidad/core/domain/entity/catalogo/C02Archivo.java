package com.universidad.core.domain.entity.catalogo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.universidad.core.domain.entity.transaccional.T03Estatus;
import com.universidad.core.domain.entity.transaccional.T04TipoArchivo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "C02Archivo")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class C02Archivo implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "idC02Archivo")
	private Integer idC02Archivo;
	@Basic(optional = false)
	@Column(name = "extencion")
	private String extencion;
	@Basic(optional = false)
	@Column(name = "mineType")
	private String mineType;
	@Basic(optional = false)
	@Column(name = "content")
	private String content;
	@Basic(optional = false)
	@Column(name = "fechaCarga")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCarga;
	@JsonIgnore
	@JoinColumn(name="tipoArchivoId")
	@ManyToOne(optional = false , cascade = CascadeType.ALL , fetch = FetchType.LAZY)
	private T04TipoArchivo tipoArchivoId;
	@JsonIgnore
	@JoinColumn(name="usuarioId")
	@ManyToOne(optional = false ,cascade = CascadeType.ALL , fetch = FetchType.LAZY)
	private C01Usuario usuarioId;
	@JsonIgnore
	@JoinColumn(name="estatusId")
	@ManyToOne(optional = false , cascade = CascadeType.ALL  ,fetch = FetchType.LAZY)
	private T03Estatus estatusId ;

}
