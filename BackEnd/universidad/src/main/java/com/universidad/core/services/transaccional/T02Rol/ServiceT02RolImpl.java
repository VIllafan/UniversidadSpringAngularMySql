package com.universidad.core.services.transaccional.T02Rol;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.universidad.core.domain.entity.transaccional.T01Perfil;
import com.universidad.core.domain.entity.transaccional.T02Rol;
import com.universidad.core.repository.transaccional.IT02RolRespository;

@Service
public class ServiceT02RolImpl implements IServiceT02Rol {
@Autowired
private IT02RolRespository it02RolRespository;

@Override
public T02Rol findById(Integer id) {
	// TODO Auto-generated method stub
	return this.it02RolRespository.findById(id).get();
}

@Override
public T02Rol findAllByPerfil(T01Perfil perfil) {
	// TODO Auto-generated method stub
	return null;
}


	
}
