package com.universidad.core.controller.catalogo.C02Archivo;

import java.util.Base64;
import java.util.Date;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.universidad.core.domain.entity.catalogo.C01Usuario;
import com.universidad.core.domain.entity.catalogo.C02Archivo;
import com.universidad.core.domain.entity.transaccional.T03Estatus;
import com.universidad.core.domain.entity.transaccional.T04TipoArchivo;
import com.universidad.core.services.catalogo.C01Usuario.IServiceC01Usuario;
import com.universidad.core.services.catalogo.C02Archvo.IServiceC02Archivo;
import com.universidad.core.services.transaccional.T03Estatus.IServiceT03Estatus;
import com.universidad.core.services.transaccional.T04TipoArchivo.IServiceT04TipoArchivo;
import com.universidad.core.util.Enum.EClavesT03Estatus;
import com.universidad.core.util.Enum.EClavesT04TipoArchivo;

@RestController
@CrossOrigin("*")
@RequestMapping("/sgu/C02Archivo")
public class C02ArchivoController {
	private static final Logger LOG = LogManager.getLogger();
	@Autowired
	private IServiceC02Archivo iServiceC02Archivo;
	@Autowired
	private IServiceC01Usuario iServiceC01Usuario;
	@Autowired
	private IServiceT04TipoArchivo iServiceT04TipoArchivo;
	@Autowired
	private IServiceT03Estatus iServiceT03Estatus;

	@PostMapping("/savePhotoToCredential")
	public ResponseEntity<?> savePhotoToCredential(@RequestParam("file") MultipartFile file,
			@RequestParam("fileName")String fileName,
			@RequestParam("type")String type,
			@RequestParam("usuarioId") Integer usuarioId) {
		C02Archivo archivo;
		C01Usuario usuario;
		T04TipoArchivo tipoArchivo;
		T03Estatus estatus;
		String b64Content;
		String mineType;
		String extencion;
		try {
			usuario = this.iServiceC01Usuario.findById(usuarioId);
			tipoArchivo = this.iServiceT04TipoArchivo.findById(EClavesT04TipoArchivo.FOTO_CREDENCIAL.getValorInt());
			estatus = this.iServiceT03Estatus.findById(EClavesT03Estatus.APROVADO.getValorInt());
			mineType = type;
			extencion = FilenameUtils.getExtension(fileName);
			b64Content = Base64.getEncoder().encodeToString(file.getBytes());
			archivo = C02Archivo.builder().tipoArchivoId(tipoArchivo).extencion(extencion).fechaCarga(new Date())
					.mineType(mineType).content(b64Content).usuarioId(usuario).estatusId(estatus).build();
			archivo = this.iServiceC02Archivo.save(archivo);
			return new ResponseEntity<C02Archivo>(archivo, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}

}
