package com.universidad.core.services.catalogo.C02Archvo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.universidad.core.domain.entity.catalogo.C02Archivo;
import com.universidad.core.repository.catalogo.IC02ArchivoRepository;
import org.apache.commons.io.FilenameUtils;

@Service
public class ServiceC02ArchivoImpl implements IServiceC02Archivo{
@Autowired
private IC02ArchivoRepository iC02ArchivoRepository;
	@Override
	public C02Archivo findById(Integer id) {
		// TODO Auto-generated method stub
		return this.iC02ArchivoRepository.findById(id).get();
	}

	@Override
	public C02Archivo save(C02Archivo archivo) {
		// TODO Auto-generated method stub
		return this.iC02ArchivoRepository.save(archivo);
	}

	@Override
	public String getMineType(String fileName) {
		// TODO Auto-generated method stub
		return this.evalueMineType(FilenameUtils.getExtension(fileName));
	}
	private String evalueMineType(String extencion) {
        String mineType = "";
        switch (extencion) {
            case "pdf":
                mineType += "application/pdf";
                break;
            case "xps":
                mineType += "application/oxps, application/vnd.ms-xpsdocument";
                break;
            case "xls":
                mineType += "application/vnd.ms-excel";
                break;
            case "xlsx":
                mineType += "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                break;
            case "xml":
                mineType += "application/xml";
                break;
            case "csv":
                mineType += "text/csv";
                break;
            case "doc":
                mineType += "application/msword";
                break;
            case "docx":
                mineType += "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                break;
            case "ppt":
                mineType += "application/vnd.ms-powerpoint";
                break;
            case "pptx":
                mineType += "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                break;
            case "svg":
                mineType += "image/svg+xml";
                break;
            case "pptm":
                mineType += "application/vnd.ms-powerpoint.presentation.macroEnabled.12";
                break;
            case "txt":
                mineType += "text/plain";
                break;
            case "avi":
                mineType += "video/x-msvideo";
                break;
            case "mp4":
                mineType += "video/mp4";
                break;
            case "mkv":
                mineType += "video/x-matroska";
                break;
            case "flv":
                mineType += "video/x-flv";
                break;
            case "mov":
                mineType += "video/quicktime";
                break;
            case "wmv":
                mineType += "video/x-ms-wmv";
                break;
            case "jpeg":
                mineType += "image/jpeg";
                break;
            case "jpg":
                mineType += "image/bmp";
                break;
            case "gif":
                mineType += "image/gif";
                break;
            case "png":
                mineType += "image/png";
                break;
            case "raw":
                mineType += "image/x-panasonic-raw";
                break;
            case "bmp":
                mineType += "image/bmp";
                break;
            case "tiff":
                mineType += "image/tiff";
                break;
            default:
                mineType += "application/octet-stream";
        }
        return mineType;
    }
}
