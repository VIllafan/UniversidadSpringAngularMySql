import { CryptographyService } from './../../Services/Security/cryptography.service';
import { C01Usuario } from './../../Model/Catalogo/c01-usuario';
import { C02ArchivoService } from './../../Services/Catalogo/c02-archivo.service';
import { C01UsuarioService } from './../../Services/Catalogo/c01-usuario.service';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-uploader-credential',
  templateUrl: './uploader-credential.component.html',
  styleUrls: ['./uploader-credential.component.css']
})
export class UploaderCredentialComponent implements OnInit {

  constructor(private c01UsusarioService : C01UsuarioService,private c02ArchivoService : C02ArchivoService,private cryptographyService : CryptographyService) { }
selectedFile : File
usuarioId
b64Content
private usuario :C01Usuario
  ngOnInit(): void {
    let token = sessionStorage.getItem('token')
    let payload = token.split('.')[1]
    let decriptedPayload = window.atob(payload)
    let credential = JSON.parse(decriptedPayload)
    let correoUsuario = this.cryptographyService.aesDecrypt(credential.sub)
    this.c01UsusarioService.findByCorreo(correoUsuario).subscribe(response=>{
      this.usuario = response
    },exception=>{
      Swal.fire({
        title : 'error',
        text : exception.error,
        icon : 'error'
      })
    })

  }
onfileSelected(event){
  if(event.target.files.length > 0){
this.selectedFile = event.target.files[0]
  }
}
async saveFile(){
let base64Content = await this.c02ArchivoService.getBase64Format(this.selectedFile)


  this.c02ArchivoService.saveFile(this.selectedFile,this.usuario.idUsuario,base64Content).subscribe(response=>{
    Swal.fire({
      title : 'exito',
      icon : 'success'
    })
  },exception =>{
    Swal.fire({
      title : 'error',
      text : exception.error,
      icon : 'error'
    })
  })
}
}
