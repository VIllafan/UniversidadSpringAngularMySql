import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploaderCredentialComponent } from './uploader-credential/uploader-credential.component';



@NgModule({
  declarations: [UploaderCredentialComponent],
  imports: [
    CommonModule
  ], 
  exports : [UploaderCredentialComponent]
})
export class UploadComponentsModule { }
