import { C01Usuario } from './../Catalogo/c01-usuario';
import { T01Perfil } from './t01-perfil';
export class T02Rol {
    
	  idT02Rol:number
	  nombre:string
	  descripcion:string
	 usuarios: C01Usuario []
	  perfilId:T01Perfil
}
