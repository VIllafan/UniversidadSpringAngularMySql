import { C01Usuario } from './../Catalogo/c01-usuario';
import { T02Rol } from "./t02-rol"

export class T01Perfil {

    idT01Perfil: number
    nombre: string
    usuarios: C01Usuario[]
    rols: T02Rol[]
}
