import { T03Estatus } from '../Transactional/t03-estatus';
import { T02Rol } from '../Transactional/t02-rol';
import { T01Perfil } from '../Transactional/t01-perfil';
export class C01Usuario {
   
    idUsuario:number
    nombre:string
    aPaterno:string
    aMaterno:string
    password:string
    correo:string
    matricula:string
    perfilId:T01Perfil
    rolId:T02Rol
   estatusId:T03Estatus
}
