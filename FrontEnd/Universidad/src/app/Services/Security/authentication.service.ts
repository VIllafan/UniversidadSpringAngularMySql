import { CryptographyService } from './cryptography.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient,private cryptographyService : CryptographyService) { }
  private URL = 'security/'



  autentication(username: string, password: string) {
    console.log('estamos en el metodo del servicio')
    let encryptedUsername = this.cryptographyService.aesEncrypt(username)
    let encryptedPassword = this.cryptographyService.aesEncrypt(password)
    username = window.btoa(encryptedUsername)
    password = window.btoa(encryptedPassword)
    let body = {
      'username': username,
      'password': password
    }
    let opt = {
      params : {
        'username': username,
        'password': password
      }
    }
    return this.http.post<string>(this.URL + 'authentication', body)

  }
}
