import { CryptographyService } from './cryptography.service';
import { Injectable } from '@angular/core';
import { catchError, filter, map, tap } from 'rxjs/operators';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpParams
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class TokenIntercepterInterceptor implements HttpInterceptor {
private URL  = 'http://localhost:8080/sgu/'
private SECURITY = true
  constructor(private cryptographyService :CryptographyService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let token  = sessionStorage.getItem('token')
    let params = request.params
    request.body
    let encryptedParams = this.cifrarParametros(params)
    let apiRequest
    if(token){
       apiRequest = request.clone({url : this.URL + request.url,setHeaders : {Authorization : token},params : encryptedParams})
      return next.handle(apiRequest);
    }else{
     apiRequest = request.clone({url : this.URL + request.url,params : encryptedParams})
    }
    return next.handle(apiRequest)
  }

  cifrarParametros(params : HttpParams){
    if(this.SECURITY){
      for(let p of params.keys()){
       if(p=='file'){
         
       }else{
        let paramKey:string = p 
        let originParam = params.get(paramKey)
        let encryptedParam:string = window.btoa(this.cryptographyService.aesEncrypt(originParam))
        params =params.set(paramKey,encryptedParam)
        console.log(`Se encripto el parametro ${paramKey}  de ${originParam}  a  ${encryptedParam}`)
       }
      }
      return params
    }else{
      return params
    }
  }
}
