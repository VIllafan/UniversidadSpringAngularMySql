import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js'

@Injectable({
  providedIn: 'root'
})
export class CryptographyService {
private KEY = 'MbxnyGyQ56rSZqli'
private SECRECT_CODE = CryptoJS.enc.Utf8.parse(this.KEY)
  constructor() { }

  aesEncrypt(text : string){
    let srcs = CryptoJS.enc.Utf8.parse(text)
    let encrypted = CryptoJS.AES.encrypt(srcs , this.SECRECT_CODE , {mode:CryptoJS.mode.ECB,padding : CryptoJS.pad.Pkcs7})
    return encrypted.toString()
  }
  aesDecrypt(text:string){
      let decrypt = CryptoJS.AES.decrypt(text,this.SECRECT_CODE,{mode:CryptoJS.mode.ECB,padding : CryptoJS.pad.Pkcs7})
      return CryptoJS.enc.Utf8.stringify(decrypt).toString()
  }
}
