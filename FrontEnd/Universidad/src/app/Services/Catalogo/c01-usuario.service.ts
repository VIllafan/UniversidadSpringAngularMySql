import { C01Usuario } from './../../Model/Catalogo/c01-usuario';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class C01UsuarioService {
private URL =  'C01Usuario/'
  constructor(private http : HttpClient) { }



  findByCorreo(correo :string){
    let opt = {
      params : {
        'correo' : correo
      }
    }
    return this.http.get<C01Usuario>(this.URL+'findByCorreo' , opt)
    
  }

  registerNewUserAsAlumno(correo,nombre,aPaterno,aMaterno){
    let opt = {
      params : {
        'correo' : correo,
        'nombre' : nombre,
        'aPaterno' : aPaterno,
        'aMaterno' : aMaterno
      }
    }
    return this.http.post<C01Usuario>(this.URL+'registerNewUserAsAlumno',null , opt)
  }
}
