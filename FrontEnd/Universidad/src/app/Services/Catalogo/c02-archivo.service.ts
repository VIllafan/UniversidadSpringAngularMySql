import { HttpClient } from '@angular/common/http';
import { unsupported } from '@angular/compiler/src/render3/view/util';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class C02ArchivoService {
  private URL = "C02Archivo/"
  constructor(private http: HttpClient) { }


   saveFile(file: File, usarioId,fileBase64Content) {

    let opt = {
      params: {
        'usuarioId': usarioId,
        'fileName': file.name,
        'type' : file.type
        
      }
    }
    const formData = new FormData();
    formData.append('file',file)
    return this.http.post<any>(this.URL + 'savePhotoToCredential', formData, opt)
  }

  getBase64Format(file : File):Promise<string>{
    return new Promise((resolve,reject)=>{
      const reader = new FileReader();
      reader.readAsDataURL(file)
      reader.onload = ()=>resolve(reader.result.toString())
      reader.onerror = error =>(reject(error))
   })
  }
}
