import { UploadComponentsModule } from './upload-components/upload-components.module';
import { FormsModule } from '@angular/forms';
import { TokenIntercepterInterceptor } from './Services/Security/token-intercepter.interceptor';
import { LoginComponentsModule } from './login-components/login-components.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


@NgModule({
  declarations: [
    AppComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LoginComponentsModule,
    HttpClientModule,
    FormsModule,
    UploadComponentsModule
  ],
  providers: [
    {provide : HTTP_INTERCEPTORS,useClass : TokenIntercepterInterceptor,multi:true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
